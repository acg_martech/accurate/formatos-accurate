{'hotelId': '116942',
 'destinationId': '1634154',
 'reviews': {'scale': 10.0, 'rating': 8.8, 'total': 153},
 'amenities': {'In the hotel': [{'heading': 'Food and drink',
                                 'listItems': ['Buffet breakfast daily (surcharge)',
                                               'Restaurant',
                                               'Bar/lounge',
                                               'Coffee shop/café',
                                               'Snack bar/deli',
                                               '24-hour room service',
                                               'Coffee/tea in a common area']},
                                {'heading': 'Things to do',
                                 'listItems': ['Fitness facilities',
                                               'Spa services on site',
                                               'Spa tub',
                                               'Steam room',
                                               'Sauna']},
                                {'heading': 'Working away',
                                 'listItems': ['24-hour business center',
                                               'Meeting rooms',
                                               'Conference center']},
                                {'heading': 'Services',
                                 'listItems': ['24-hour front desk',
                                               'Concierge services',
                                               'Tours/ticket assistance',
                                               'Dry cleaning/laundry service',
                                               'Laundry facilities',
                                               'Luggage storage',
                                               'Multilingual staff',
                                               'Porter/bellhop']},
                                {'heading': 'Facilities',
                                 'listItems': ['Number of buildings/towers -  1',
                                               'Year Built -  1978',
                                               'Elevator',
                                               'Terrace']},
                                {'heading': 'Accessibility',
                                 'listItems': ['Braille or raised signage',
                                               'In-room accessibility',
                                               'Wheelchair-accessible registration desk',
                                               'Braille signage',
                                               'Raised toilet seat',
                                               'Grab bar - near toilet',
                                               'Bathroom emergency pull cord']},
                                {'heading': 'Languages Spoken', 'listItems': ['English', 'Spanish']}],
               'In the room': [{'heading': 'Home comforts',
                                'listItems': ['Air conditioning',
                                              'Minibar',
                                              'Slippers',
                                              'Iron/ironing board']},
                               {'heading': 'Sleep well',
                                'listItems': ['Hypo-allergenic bedding available',
                                              'Pillow menu',
                                              'Down comforter',
                                              'Blackout drapes/curtains',
                                              'Soundproofed rooms',
                                              'Premium bedding',
                                              'Memory foam mattress']},
                               {'heading': 'Freshen up',
                                'listItems': ['Private bathroom',
                                              'Shower only',
                                              'Free toiletries',
                                              'Hair dryer']},
                               {'heading': 'Be entertained',
                                'listItems': ['32-inch LCD TV',
                                              'Cable TV channels',
                                              'IPod docking station']},
                               {'heading': 'Stay connected', 'listItems': [
                                   'Desk', 'Free WiFi', 'Phone']},
                               {'heading': 'Food and drink',
                                'listItems': ['Refrigerator']},
                               {'heading': 'More', 'listItems': ['Daily housekeeping', 'In-room safe']}]}}
